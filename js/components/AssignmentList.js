import Assignment from './Assignment.js';
import AssignmentTags from './AssignmentTags.js';
import Panel from './Panel.js';

export default {
    components: { Assignment, AssignmentTags, Panel },

    template: `
        <panel v-show="show && assignments.length">
            <div class="d-flex justify-content-between align-items-start">
                <h2>{{ title }} ({{ assignments.length }})</h2>
                <button class="btn btn-outline-secondary btn-sm" v-show="canToggle" @click="show = false">&times;</button>
            </div>
            <assignment-tags :initialTags="assignments.map(a => a.tag)" v-model:currentTag="currentTag"></assignment-tags>
            <ul>
                <li v-for="assignment in filteredAssignments" :key="assignment.id">
                    <assignment :assignment="assignment"></assignment>
                </li>
            </ul>

            <slot></slot>
        </panel>
    `,

    data() {
        return {
            currentTag: 'all',
            show: true,
        }
    },

    props: {
        assignments: Array,
        title: String,
        canToggle: {
            type: Boolean,
            default: false,
        }
    },

    computed: {
        filteredAssignments() {
            if (this.currentTag === 'all') 
            {
                return this.assignments;
            }
            return this.assignments.filter(a => a.tag === this.currentTag);
        },
    }
}