export default {
    template: `
        <div class="pb-2">
            <button class="btn btn-sm btn-outline-info" :class="{'active': currentTag == tag}" v-for="tag in tags" @click="$emit('update:currentTag', tag)">{{ tag }}</button>
            <button class="btn btn-sm btn-outline-info" :class="{'active': currentTag == 'all'}" @click="$emit('update:currentTag', 'all')">all</button>
        </div>
    `,

    props: {
        initialTags: Array,
        currentTag: String
    },

    data() {
        return {
        }
    },

    computed: {
        tags() {
            return new Set(this.initialTags);
        },
    }
}