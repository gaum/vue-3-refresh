export default {
    template: `
        <label>
            {{ assignment.name }}
            <input type="checkbox" v-model="assignment.completed">
        </label>
    `,

    props: {
        assignment: Object
    }
}