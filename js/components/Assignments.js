import AssignmentList from "./AssignmentList.js";
import AssignmentCreate from "./AssignmentCreate.js";

export default {
    components: { AssignmentList, AssignmentCreate },
    template: `
        <div class="row">
            <assignment-list class="col-2 border m-3 p-2" :assignments="filters.inProgress" title="In Progress">
                <hr>
                <assignment-create @add="add"></assignment-create>
            </assignment-list>
            <assignment-list class="col-2 border m-3 p-2" :assignments="filters.completed" :canToggle="true" title="Completed"></assignment-list>
        </div>
    `,

    data() {
        return {
            assignments: [],
        }
    },

    created() { 
        fetch('http://localhost:3001/assignments')
            .then(response => response.json())
            .then(data => this.assignments = data);
    },

    computed: {
        filters() {
            return {
                inProgress: this.assignments.filter(a => !a.completed),
                completed: this.assignments.filter(a => a.completed)
            }
        }
    },

    methods: {
        add(name) {
            this.assignments.push({
                name: name,
                completed: false,
                id: this.assignments.length + 1
            });
        }
    },
}