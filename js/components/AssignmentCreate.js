export default {
    template: `
        <form @submit.prevent="add">
            <input type="text" placeholder="Add Assignment" v-model="newAssignment">
            <button class="btn btn-success" type="submit">Add</button>
        </form>    
    `,

    data() {
        return {
            newAssignment: ''
        }
    },

    methods: {
        add() {
            this.$emit('add', this.newAssignment);
            this.newAssignment = '';
        }
    }
}