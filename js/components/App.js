import Assignments from './Assignments.js';
import Panel from './Panel.js';

export default {
    components: { Assignments, Panel },

    template: `
        <assignments></assignments>

        <panel>
            <template #heading>
                Named Slot Heading
            </template>
            Default slot content
        </panel>
    `,
    
};