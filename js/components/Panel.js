export default {
    template: `
        <div class="col-2 bg-secondary-subtle border rounded p-2">
            <h3>
                <slot name="heading"></slot>
            </h3>

            <slot></slot>
        </div>
    `,

}